# Swift Style Guide WIP



- Braces 
	- Opening and Closing
	- Control Flow
- Comments
	- Inline Comments
	- Pragma Marks
- Guards 
- Optionals
	- Optional Binding
	- Optional Chaining
	- Avoid Implicitly Unwrapped Optionals
- Classes, Structs, Protocols
	- Structs Over Classes 
	- Protocols
	- Type Inference
- Dictionaries and Arrays
	- Dictionary/Array over NSDictionary/NSArray 
	- Dictionary/Array Construction
- Misc Styling
	- Avoid ++ and -- operators
	- Colon Space
	- If statements
	- If Parentheses
	- Descriptive Names
	- Self 
	- WeakSelf
	- DLog
- WIP


🤔 = in discussion

	
# Braces 

### Opening and Closing

Braces should open on the same line as their code block, followed by an empty line.

Closing braces should appear after the last line of the code block

**Prefer this**

~~~swift
func pingpongChampion() -> Dev {

	return Will
}
~~~

**Over this**

~~~swift
func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
	print(UIApplication.accessibilityValue())
	return true
}
~~~

**Why?** Code blocks with many parameters and/or long parameter names can be difficult to read, particularly if these values are used straight away. For example:

### Control Flow

Follow the same opening and closing style in control flow statements, and include line breaks break control flow keywords. For example:

**Prefer this**

~~~swift
if Time.lunchTime {

	Devs.popping += [Antonio, Jacek, Sushma]
}
else {

	Devs.popping.removeAll()
}
~~~

**Over this**

~~~swift
if Time.lunchTime {
	
	Devs.popping += [Antonio, Jacek, Sushma]
} else {

	Devs.popping.removeAll()
}
~~~

**Why?** Keywords directly following brackets makes the code more difficult to read and harder to select.


# Comments


## Inline Comments

Comments should always contain a space after `//`

**Prefer this**

~~~swift
// some comment
~~~

**Over this**

~~~swift
//lil comment
~~~

**Why?** Depending on font and font size, it could be difficult to read the first character / first word of the comment.

## Pragma Marks

Always use `// MARK: - ` to categorise functionality in to logical groups.

Only use `// MARK: ` to subcategorise if necessary.

~~~swift
// MARK: - Shapes
func numberOfSides() -> Int { //some code }
func area() -> Double { //some code }

// MARK: Circles
func radius() -> Double { // some code }
func diameter() -> Double { // some code }
~~~

# Guards

Make use of `guard` where possible.

Guard allows unwrapped optionals to remain in scope after the guard.

Guard checks the condition you want, instead of the one you do not.
 
**Prefer this**

~~~swift
func printMiddleName(middleName: String?) {

	guard let middleName = middleName else {
		
		// there is not a middle name
		return
	}
	
	// we can use middleName safely without unwrapping
	print ("Middle name is \(middleName)")
}
~~~

**Over this**

~~~swift 
func printMiddleName(middleName: String?) {

	if let middleName = middleName {
		
		print("Middle name is \(midName)")
	}
	
	// there is not a middlename
}
~~~



# Optionals

## Optional Binding

Use optional binding instead of checking for nil.

**Prefer this with `Guard`**

~~~swift
let pinpongChampion: Dev?

	guard let pingpongChampion = pingpongChampion else {
	
	return
}

// some code
~~~

**Alternatively without `Guard` (e.g. `if/else`)**

~~~swift
let pinpongChampion: Dev?

if let pingpongChampion = pingpongChampion {
	
	// Some code
}
~~~

**Over this**

~~~swift
let pinpongChampion: Dev?

if pingpongChampion != nil {

	// Some code using unwrapped pingpongChampion!
}
~~~

## Optional Chaining 🤔🤔🤔

Avoid using optional chaining unless the value is accessed only once.

**Prefer this**

~~~swift
if let PingPongScoreView = PingPongScoreView {

	PingPongScoreView.update(10, 10)
	PingPongScoreView.scoreLabel?.animate()
}

~~~
**Over this**

~~~swift
PingPongScoreView?.update(10, 10)
PingPongScoreView?.scoreLabel?.animate()
~~~

**Why?** It can be hard to understand what is happening when there are multiple `?` and `!` in a chain.

## Avoid Implicitly Unwrapped Optionals

Avoid using implicitily unwrapped optionals if there is a possibility for a value to be nil, instead unwrap optionals safely when needed.

**Prefer this**

~~~swift
var x: String
var x: String?
~~~

**Over this**

~~~swift
var x: String!
~~~

**Why?** Implicitily unwrapped optionals can lead to unsafe code, and have the potential of crashing at runtime.

# Classes, Structs, Protocols, Types

## Structs Over Classes

Prefer structs over classes.

Value types (e.g. Strings, Dictionaries) have simpler behaviour when compared to objects, and behave as expected with the `let` keyword.


## Protocols 

When conforming to protocols, implement using class extensions.

**Prefer**

~~~swift
class FlingMapController: UIViewController {

	// some code
}

// Mark: - MKMapViewDelegate
extension FlingMapController: MKMapViewDelegate {

	// map kit methods
}
~~~
**Over**

~~~swift
class FlingMapController: UIViewController, MKMapViewDelegate {
	
	// all methods 
}
~~~

**Why?** Keeps the protocol methods grouped together logically


## Type Inference 

Make use of Swift inference where possible, unless you need a specific type (e.g. `UInt64` instead of `Int`) 

**Prefer this**

~~~swift
var pingpongChampion = Sushma
~~~

**Over this**

~~~swift
var pinpongChampion: Dev = Sushma
~~~

**Why?** Using inference helps to keep code compact, and improves readability.




# Dictionaries and Arrays


## Dictionary/Array over NSDictionary/NSArray

`array` and `dictionary` are structs in Swift that are bridged to their Cocoa equivalent classes `NSArray` and `NSDictionary`, and `NSMutableArray` and `NSMutableDictionary`.

As we are preferring structs over classes, make use of swift `array` and `dictionary` types.

**Prefer this**

~~~swift
var x = [1,2,3] 		
var y = x 	// [1,2,3]
x += [4]

print(x)	// [1,2,3,4]
print(y)	// [1,2,3]
~~~

**Over this**

~~~swift
var a = NSMutableArray(array: [1,2,3])
var b = NSMutableArray()
b = a		// [1, 2, 3]

a.insertObject(4, atIndex: 3)

print(a)	// [1,2,3,4]
print(b)	// [1,2,3,4]
~~~

## Dictionary/Array Construction 

Prefer the Swift short-hand when using arrays and dictionaries.

**Prefer this**

~~~swift
// array
var x: [UInt64]
// init an empty array
var x = [UInt64]()
// init an array with some values
var x = [UInt64](count: 5, repeatedValue: 37)

// dictionary
var y: [Int: String]
// init an empty dictionary
var y = [Int: String]()
~~~

**Over this**

~~~swift
var x: Array<Int>
var x = Array<Int>()
// or
var y: Dictionary<Int, String>
var y = Dictionary<Int, String>()
~~~

# Misc. Styling

## Avoid ++ and -- operators 

`++` and `--` are deprecated and will be removed from the next version of Swift.

**Prefer this**

~~~swift
x += 1
y -= 1
~~~

**Over this**

~~~swift
x++
y--
~~~

## Comma Space 

Always follow commas with a space

~~~swift
let x: (Int, Int) = (17, 22)
startGame(player1: Will, player2: Jacek)
~~~

## Colon Space 

Always follow colons with a space

~~~swift
let x: Dev = Dev()
let y: [Int: String]
~~~

# If Statements

Favor always the positive story.

 **Prefer this**

~~~swift
if booleanVariable == true {

	// some Code
} 
else {
   
	// Some code
}
~~~

**Over this**

~~~swift
if booleanVariable == false {

	// some Code
}
else {
   
	// Some code
}
~~~

## If Parentheses 

Avoid using parentheses in `if` statements.

**Prefer this**

~~~swift
// Prefer
if x > 5 { // some code }
~~~
**Over this**

~~~swift
if (x > 5) { // some code }
~~~

**Why?** Consistency with `for in` loops

## Descriptive Names 

Always use descriptive names.

**Prefer this**

~~~swift
if sendMessageButton.hasBeenPressed { // some code }
~~~
**Over this**

~~~swift
if button.action { // some code }
~~~

**Why?** Autocomplete allows us to use longer, more descriptive names that avoid ambiguity.


## Self

Avoid using `self` as it is not required to access an object's properties or methods. 

Only use `self` when needed to differentiate between property names and init arguments or in closures (the compiler will make these obvious).

~~~swift
class PingpongGame {

	var duration: Double
	
	init(duration: Double) { 
	
		self.duration = duration		
	}
}
~~~

## WeakSelf

When creating a weak self reference the shortest version is preferred.

**Prefer this**

~~~swift
[weak self]
~~~
**Over these**

~~~swift
[weak weakSelf = self]
[weak self = self]
~~~

## DLog over print

NSLog in the form of `DLog` should be used for debug logs as it is optimised for working in multithreaded applications.
Despite being more performant, `print` does not always behave as expected.

**Prefer this**

~~~swift
DLog("This is the preferred way to log")
~~~
**Over this**

~~~swift
print("This log should be avoided")
~~~

---
---
---
---
---
---
---
---
---

## Properties - WIP

lazy
private
let / var


## Parameters - inline vs stacked





## Final keyword



---
---
---
---
---
